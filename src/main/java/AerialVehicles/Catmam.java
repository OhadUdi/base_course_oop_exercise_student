package AerialVehicles;

import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.FlightStatus;

public abstract class Catmam extends AerialVehicle {
    public Catmam(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes[] abilitiesTypes) {
        super(name, hoursPassedSinceLastFix, flightStatus, sourceBaseCoordinates, abilitiesTypes);
    }

    public String hoverOverLocation(Coordinates destination) {
        String message = "This Catmam doesn't hovering..";
        if (this.flightStatus == FlightStatus.OnTheAir) {
            message = "Hovering Over: " + destination.getLongitude() + " , " + destination.getLatitude();
        }

        return message;
    }
}
