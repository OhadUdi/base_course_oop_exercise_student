package AerialVehicles;

import Entities.Coordinates;
import Enums.*;

public class Kochav extends Hermes{
    private int rocketsAmount;
    private RocketKind rocketKind;
    private SensorKind sensorKind;
    private CameraKind cameraKind;

    public Kochav(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes[] abilitiesTypes, int rocketsAmount, RocketKind rocketKind, SensorKind sensorKind, CameraKind cameraKind) {
        super(name, hoursPassedSinceLastFix, flightStatus, sourceBaseCoordinates, abilitiesTypes);
        this.rocketsAmount = rocketsAmount;
        this.rocketKind = rocketKind;
        this.sensorKind = sensorKind;
        this.cameraKind = cameraKind;
    }

    public int getRocketsAmount() {
        return rocketsAmount;
    }

    public RocketKind getRocketKind() {
        return rocketKind;
    }

    public CameraKind getCameraKind() {
        return cameraKind;
    }

    public SensorKind getSensorKind() {
        return sensorKind;
    }
}
