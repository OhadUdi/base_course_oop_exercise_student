package AerialVehicles;


import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.CameraKind;
import Enums.FlightStatus;
import Enums.SensorKind;

public class Zik extends Hermes {
    private SensorKind sensorKind;
    private CameraKind cameraKind;

    public Zik(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes[] abilitiesTypes, SensorKind sensorKind, CameraKind cameraKind) {
        super(name, hoursPassedSinceLastFix, flightStatus, sourceBaseCoordinates, abilitiesTypes);
        this.sensorKind = sensorKind;
        this.cameraKind = cameraKind;
    }

    public CameraKind getCameraKind() {
        return cameraKind;
    }

    public SensorKind getSensorKind() {
        return sensorKind;
    }
}