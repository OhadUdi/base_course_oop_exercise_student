package AerialVehicles;


import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.CameraKind;
import Enums.FlightStatus;
import Enums.RocketKind;

public class F16 extends Fight{
    private int rocketsAmount;
    private RocketKind rocketKind;
    private CameraKind cameraKind;

    public F16(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes[] abilitiesTypes, int rocketsAmount, RocketKind rocketKind, CameraKind cameraKind) {
        super(name, hoursPassedSinceLastFix, flightStatus, sourceBaseCoordinates, abilitiesTypes);
        this.rocketsAmount = rocketsAmount;
        this.rocketKind = rocketKind;
        this.cameraKind = cameraKind;
    }

    public int getRocketsAmount() {
        return rocketsAmount;
    }

    public RocketKind getRocketKind() {
        return rocketKind;
    }

    public CameraKind getCameraKind() {
        return cameraKind;
    }
}
