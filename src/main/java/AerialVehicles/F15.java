package AerialVehicles;

import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.FlightStatus;
import Enums.RocketKind;
import Enums.SensorKind;

public class F15 extends Fight{
        private int rocketsAmount;
        private RocketKind rocketKind;
        private SensorKind sensorKind;

    public F15(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes[] abilitiesTypes, int rocketsAmount, RocketKind rocketKind, SensorKind sensorKind) {
        super(name, hoursPassedSinceLastFix, flightStatus, sourceBaseCoordinates, abilitiesTypes);
        this.rocketsAmount = rocketsAmount;
        this.rocketKind = rocketKind;
        this.sensorKind = sensorKind;
    }

    public int getRocketsAmount() {
        return rocketsAmount;
    }

    public RocketKind getRocketKind() {
        return rocketKind;
    }

    public SensorKind getSensorKind() {
        return sensorKind;
    }
}
