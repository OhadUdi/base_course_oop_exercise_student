package AerialVehicles;

import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.FlightStatus;

public abstract class AerialVehicle {
    protected String name;
    protected Double hoursPassedSinceLastFix;
    protected FlightStatus flightStatus;
    protected Coordinates sourceBaseCoordinates;
    protected AbilitiesTypes[] abilitiesTypes;
    static final int LIMIT_DAYS_HARON = 150;
    static final int LIMIT_DAYS_HERMES = 100;
    static final int LIMIT_DAYS_FIGHT = 250;


    public AerialVehicle(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes [] abilitiesTypes) {
        this.name = name;
        this.hoursPassedSinceLastFix = hoursPassedSinceLastFix;
        this.flightStatus = flightStatus;
        this.sourceBaseCoordinates = sourceBaseCoordinates;
        this.abilitiesTypes = new AbilitiesTypes[abilitiesTypes.length];
        for (int i = 0; i < abilitiesTypes.length; i++) {
            this.abilitiesTypes[i] = abilitiesTypes[i];
        }
    }

    public void flyTo(Coordinates destination) {
        if (this.flightStatus == FlightStatus.Ready) {
            System.out.println("<Flying to: " + destination.getLongitude() + " , " + destination.getLatitude() + " >");
        } else if (this.flightStatus == FlightStatus.NotReady) {
            System.out.println("Aerial Vehicle isn't ready to fly");
        } else if (this.flightStatus == FlightStatus.OnTheAir) {
            System.out.println("I'm on the air!!!");
        }
    }

    public void land(Coordinates destination) {
        System.out.println("<Landing on: " + destination.getLongitude() + " , " + destination.getLatitude() + " >");
        check();
    }

    public void check() {
        if (this instanceof Shoval || this instanceof Eitan) {
            if (this.hoursPassedSinceLastFix >= LIMIT_DAYS_HARON) {
                this.flightStatus = FlightStatus.NotReady;
                repair();
            } else {
                this.flightStatus = FlightStatus.Ready;
            }
        } else if (this instanceof Zik || this instanceof Kochav) {
            if (this.hoursPassedSinceLastFix >= LIMIT_DAYS_HERMES) {
                this.flightStatus = FlightStatus.NotReady;
                repair();
            } else {
                this.flightStatus = FlightStatus.Ready;
            }
        } else if (this instanceof F15 || this instanceof F16) {
            if (this.hoursPassedSinceLastFix >= LIMIT_DAYS_FIGHT) {
                this.flightStatus = FlightStatus.NotReady;
                repair();
            } else {
                this.flightStatus = FlightStatus.Ready;
            }
        }
    }

    public void repair() {
        this.hoursPassedSinceLastFix = Double.valueOf(0);
        this.flightStatus = FlightStatus.Ready;
    }

    public Coordinates getSourceBaseCoordinates() {
        return this.sourceBaseCoordinates;
    }

    public String getName() {
        return this.name;
    }

    public AbilitiesTypes[] getAbilitiesTypes() {
        return abilitiesTypes;
    }
}
