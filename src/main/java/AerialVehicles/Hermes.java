package AerialVehicles;

import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.FlightStatus;

public abstract class Hermes extends Catmam {
    public Hermes(String name, Double hoursPassedSinceLastFix, FlightStatus flightStatus, Coordinates sourceBaseCoordinates, AbilitiesTypes[] abilitiesTypes) {
        super(name, hoursPassedSinceLastFix, flightStatus, sourceBaseCoordinates, abilitiesTypes);
    }
}
