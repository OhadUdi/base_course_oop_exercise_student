import AerialVehicles.AerialVehicle;
import AerialVehicles.F15;
import AerialVehicles.Kochav;
import AerialVehicles.Zik;
import Entities.Coordinates;
import Enums.*;
import Missions.*;

public class Main {
    public static void main(String [] args) {
        AerialVehicle f15 = new F15("Shimson", 24.5, FlightStatus.Ready, new Coordinates(10.2, 20.9), new AbilitiesTypes[]{AbilitiesTypes.Attack, AbilitiesTypes.Intelligence}, 20, RocketKind.Python, SensorKind.InfraRed);
        f15.check();
        Mission attackMission = new AttackMission(new Coordinates(500.3, 200.1), "Ofek Berko", f15, "House");
        attackMission.begin();
        try {
            attackMission.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }

        AerialVehicle zik = new Zik("Srak", 40.5, FlightStatus.Ready, new Coordinates(20.5, 40.9), new AbilitiesTypes[]{AbilitiesTypes.Bda, AbilitiesTypes.Intelligence}, SensorKind.InfraRed, CameraKind.Regular);
        zik.check();
        Mission intelligenceMission = new IntelligenceMission(new Coordinates(750.3, 320.1), "Yohad Levi", zik, "Hospital");
        intelligenceMission.begin();
        try {
            intelligenceMission.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }

        AerialVehicle kochav = new Kochav("Latmus", 70.5, FlightStatus.Ready, new Coordinates(60.3, 70.15), new AbilitiesTypes[]{AbilitiesTypes.Attack, AbilitiesTypes.Intelligence, AbilitiesTypes.Bda}, 32, RocketKind.Amram, SensorKind.Elint, CameraKind.Regular);
        kochav.check();
        Mission bdaMission = new BdaMission(new Coordinates(829.22, 412.12), "Stefhan Lula", kochav, "House");
        bdaMission.begin();
        try {
            bdaMission.finish();
        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }
    }
}
