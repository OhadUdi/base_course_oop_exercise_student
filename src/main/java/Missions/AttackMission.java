package Missions;

import AerialVehicles.*;
import Entities.Coordinates;
import Enums.AbilitiesTypes;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(Coordinates destinationCoordinates, String pilotName, AerialVehicle performerAerialVehicle, String target) {
        super(destinationCoordinates, pilotName, performerAerialVehicle);
        this.target = target;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        int abilitiesAmount = this.performerAerialVehicle.getAbilitiesTypes().length;
        String amountRocketsAndRocketKind = null;

        for (int i = 0; i < abilitiesAmount ; i++) {
            if (this.performerAerialVehicle.getAbilitiesTypes()[i] == AbilitiesTypes.Attack) {
                if (this.performerAerialVehicle instanceof F15) {
                    F15 f15 = (F15)this.performerAerialVehicle;
                    amountRocketsAndRocketKind = f15.getRocketKind() + "X" + f15.getRocketsAmount();
                } else if (this.performerAerialVehicle instanceof F16) {
                    F16 f16 = (F16)this.performerAerialVehicle;
                    amountRocketsAndRocketKind = f16.getRocketKind() + "X" + f16.getRocketsAmount();
                } else if (this.performerAerialVehicle instanceof Shoval) {
                    Shoval shoval = (Shoval)this.performerAerialVehicle;
                    amountRocketsAndRocketKind = shoval.getRocketKind() + "X" + shoval.getRocketsAmount();
                } else if (this.performerAerialVehicle instanceof Eitan) {
                    Eitan eitan = (Eitan) this.performerAerialVehicle;
                    amountRocketsAndRocketKind = eitan.getRocketKind() + "X" + eitan.getRocketsAmount();
                } else if (this.performerAerialVehicle instanceof Kochav) {
                    Kochav kochav = (Kochav) this.performerAerialVehicle;
                    amountRocketsAndRocketKind = kochav.getRocketKind() + "X" + kochav.getRocketsAmount();
                }
            }
        }

        if (amountRocketsAndRocketKind != null) {
            return this.pilotName + ": " + this.performerAerialVehicle.getName() + " Attacking suspect " + this.target + " with: " + amountRocketsAndRocketKind;
        } else {
            throw new AerialVehicleNotCompatibleException("this Aerial Vehicle can't do Attack missions..");
        }
    }
}
