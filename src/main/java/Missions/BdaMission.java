package Missions;

import AerialVehicles.*;
import Entities.Coordinates;
import Enums.AbilitiesTypes;
import Enums.CameraKind;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates destinationCoordinates, String pilotName, AerialVehicle performerAerialVehicle, String objective) {
        super(destinationCoordinates, pilotName, performerAerialVehicle);
        this.objective = objective;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        int abilitiesAmount = this.performerAerialVehicle.getAbilitiesTypes().length;
        String cameraKind = null;

        for (int i = 0; i < abilitiesAmount ; i++) {
            if (this.performerAerialVehicle.getAbilitiesTypes()[i] == AbilitiesTypes.Bda) {
                if(this.performerAerialVehicle instanceof F16) {
                    F16 f16 = (F16)this.performerAerialVehicle;
                    cameraKind = f16.getCameraKind() + "";
                } else if (this.performerAerialVehicle instanceof Zik) {
                    Zik zik = (Zik)this.performerAerialVehicle;
                    cameraKind = zik.getCameraKind() + "";
                } else if (this.performerAerialVehicle instanceof Shoval) {
                    Shoval shoval = (Shoval)this.performerAerialVehicle;
                    cameraKind = shoval.getCameraKind() + "";
                } else if (this.performerAerialVehicle instanceof Kochav) {
                    Kochav kochav = (Kochav)this.performerAerialVehicle;
                    cameraKind = kochav.getCameraKind() + "";
                }
            }
        }

        if (cameraKind != null) {
            return this.pilotName + ": " + this.performerAerialVehicle.getName() + " taking pictures of suspect " + this.objective + " with: " + cameraKind;
        } else {
            throw new AerialVehicleNotCompatibleException("this Aerial Vehicle can't do Bda missions..");
        }
    }

}

