package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Interfaces.IExecuteMission;

public abstract class Mission implements IExecuteMission {
    protected Coordinates destinationCoordinates;
    protected String pilotName;
    protected AerialVehicle performerAerialVehicle;

    public Mission(Coordinates destinationCoordinates, String pilotName, AerialVehicle performerAerialVehicle) {
        this.destinationCoordinates = destinationCoordinates;
        this.pilotName = pilotName;
        this.performerAerialVehicle = performerAerialVehicle;
    }

    public void begin() {
        System.out.println("Begining Mission!");
        this.performerAerialVehicle.flyTo(this.destinationCoordinates);
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        this.performerAerialVehicle.land(this.performerAerialVehicle.getSourceBaseCoordinates());
    }

    public void finish() throws AerialVehicleNotCompatibleException {
        System.out.println(this.executeMission());
        this.performerAerialVehicle.land(this.performerAerialVehicle.getSourceBaseCoordinates());
        System.out.println("Finish Mission!");
    }

}
