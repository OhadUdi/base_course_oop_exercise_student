package Missions;

import AerialVehicles.*;
import Entities.Coordinates;
import Enums.AbilitiesTypes;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates destinationCoordinates, String pilotName, AerialVehicle performerAerialVehicle, String region) {
        super(destinationCoordinates, pilotName, performerAerialVehicle);
        this.region = region;
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        int abilitiesAmount = this.performerAerialVehicle.getAbilitiesTypes().length;
        String sensorKind = null;

        for (int i = 0; i < abilitiesAmount ; i++) {
            if (this.performerAerialVehicle.getAbilitiesTypes()[i] == AbilitiesTypes.Intelligence) {
                if (this.performerAerialVehicle instanceof F15) {
                    F15 f15 = (F15)this.performerAerialVehicle;
                    sensorKind = f15.getSensorKind() + "";
                } else if (this.performerAerialVehicle instanceof Zik) {
                    Zik zik = (Zik)this.performerAerialVehicle;
                    sensorKind = zik.getSensorKind() + "";
                } else if (this.performerAerialVehicle instanceof Shoval) {
                    Shoval shoval = (Shoval)this.performerAerialVehicle;
                    sensorKind = shoval.getSensorKind() + "";
                } else if (this.performerAerialVehicle instanceof Eitan) {
                    Eitan eitan = (Eitan)this.performerAerialVehicle;
                    sensorKind = eitan.getSensorKind() + "";
                } else if (this.performerAerialVehicle instanceof Kochav) {
                    Kochav kochav = (Kochav)this.performerAerialVehicle;
                    sensorKind = kochav.getSensorKind() + "";
                }
            }
        }

        if (sensorKind != null) {
            return this.pilotName + ": " + this.performerAerialVehicle.getName() + " collecting data in " + this.region + " with: sensor type: " + sensorKind;
        } else {
            throw new AerialVehicleNotCompatibleException("this Aerial Vehicle can't do Intelligence missions..");
        }
    }
}

