package Enums;

public enum CameraKind {
    Regular,
    Thermal,
    NightVision
}
