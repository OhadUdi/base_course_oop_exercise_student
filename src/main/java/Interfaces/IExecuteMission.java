package Interfaces;

import Missions.AerialVehicleNotCompatibleException;

public interface IExecuteMission {
    String executeMission() throws AerialVehicleNotCompatibleException;
}
